import os
import pathlib
import tensorflow as tf
import numpy as np
from keras import layers

# Define variables
EPOCHS = 100
BATCH_SIZE = 32

IMG_HEIGHT = 224
IMG_WIDTH = 224
SHAPE = [IMG_HEIGHT, IMG_WIDTH]

DATA_DIR = pathlib.Path('./balanced_dataset')
model_save_folder = './model'
if not os.path.exists(model_save_folder):
    os.mkdir(model_save_folder)

IMAGE_COUNT = len(list(DATA_DIR.glob('*/*.*')))
VALIDATION_SIZE = int(IMAGE_COUNT * 0.2)

CLASS_NAMES = np.array(
    sorted([item.name for item in DATA_DIR.glob('*') if item.is_dir()])
)
NUM_CLASSES = len(CLASS_NAMES)

def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)
    one_hot = parts[-2] == CLASS_NAMES
    return one_hot

def load_image(file_path):
    label = get_label(file_path)
    img = tf.io.read_file(file_path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, SHAPE)
    img = tf.cast(img, dtype=tf.uint8)
    return img, label

dataset: tf.data.Dataset = tf.data.Dataset.list_files(str(DATA_DIR/'*/*'), shuffle=False)
dataset: tf.data.Dataset = dataset.shuffle(IMAGE_COUNT, reshuffle_each_iteration=False, seed=1337)

train_dataset = dataset.skip(VALIDATION_SIZE)
validation_dataset = dataset.take(int(VALIDATION_SIZE / 2))

train_dataset = train_dataset.map(
    load_image,
    num_parallel_calls=tf.data.experimental.AUTOTUNE
)
validation_dataset = validation_dataset.map(
    load_image,
    num_parallel_calls=tf.data.experimental.AUTOTUNE
)

def configure_dataset(ds: tf.data.Dataset, shuffle: bool):
    ds = ds.cache()
    if shuffle:
        ds = ds.shuffle(buffer_size=IMAGE_COUNT)
    ds = ds.batch(BATCH_SIZE)
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return ds

train_dataset = configure_dataset(train_dataset, False)
validation_dataset = configure_dataset(validation_dataset, False)

img_augmentation = tf.keras.Sequential(
    [
        tf.keras.layers.RandomRotation(factor=0.1),
        tf.keras.layers.RandomFlip(mode="horizontal"),
        tf.keras.layers.RandomBrightness(factor=0.1),
        tf.keras.layers.RandomZoom(height_factor=0.1, width_factor=0.1)
    ],
    name="img_augmentation",
)

def create_model():
    _base_model = tf.keras.applications.efficientnet.EfficientNetB0(
        include_top=False,
        input_shape=(IMG_WIDTH, IMG_HEIGHT, 3),
        weights='imagenet'
    )
    _base_model.trainable = False
    inputs = tf.keras.layers.Input(shape=(IMG_WIDTH, IMG_HEIGHT, 3), dtype=tf.uint8)
    x = img_augmentation(inputs)
    x = _base_model(x, training = False)
    x = tf.keras.layers.GlobalMaxPooling2D()(x)
    x = tf.keras.layers.Dropout(0.2)(x)
    x = tf.keras.layers.Dense(512, activation='relu')(x)
    x = tf.keras.layers.Dense(256, activation='relu')(x)
    x = tf.keras.layers.Dense(128, activation='relu')(x)
    x = tf.keras.layers.Dense(64, activation='relu')(x)
    outputs = tf.keras.layers.Dense(NUM_CLASSES, activation='softmax')(x)
    _model = tf.keras.Model(inputs, outputs)
    return _model, _base_model

# CREATE/COMPILE MODEL
model, base_model = create_model()
model.summary()

optimizer = tf.keras.optimizers.Adam()
optimizer.learning_rate = 1e-3 # find optimal learing rate

early_stop_callback = tf.keras.callbacks.EarlyStopping(
    patience=5, verbose=1, restore_best_weights=True
)

callbacks = [early_stop_callback]

metrics = [
    tf.keras.metrics.CategoricalAccuracy(name="accuracy"),
    tf.keras.metrics.TopKCategoricalAccuracy(5, name="top_5_accuracy")
]

model.compile(
    optimizer=optimizer,
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=metrics
)

# Train model
history = model.fit(
    train_dataset,
    validation_data=validation_dataset,
    epochs=EPOCHS,
    callbacks=callbacks,
)

np.save(f'{model_save_folder}/history.npy', history.history)

# Fine tune model
base_model.trainable = True
for layer in base_model.layers:
        if isinstance(layer, layers.BatchNormalization):
            layer.trainable = False

optimizer = tf.keras.optimizers.Adam(learning_rate=1e-5)
optimizer.learning_rate = 1e-5

model.compile(
    optimizer=optimizer,
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=metrics
)

history = model.fit(
    train_dataset,
    validation_data=validation_dataset,
    epochs=EPOCHS,
    callbacks=callbacks,
)

# Save model
np.save(f'{model_save_folder}/history_fine_tune.npy', history.history)
model.save(f'{model_save_folder}/final')
