import os
import random
import shutil

ROOT_DATASET_DIR = "dataset/"
OUTPUT_DATASET_DIR = "balanced_dataset/"

LABELS = [
    'Achaemenid architecture',
    'American craftsman style',
    'American Foursquare architecture',
    'Ancient Egyptian architecture',
    'Art Deco architecture',
    'Art Nouveau architecture',
    'Baroque architecture',
    'Bauhaus architecture',
    'Beaux-Arts architecture',
    'Byzantine architecture',
    'Chicago school architecture',
    'Colonial architecture',
    'Deconstructivism',
    'Edwardian architecture',
    'Georgian architecture',
    'Gothic architecture',
    'Greek Revival architecture',
    'International style',
    'Novelty architecture',
    'Palladian architecture',
    'Postmodern architecture',
    'Queen Anne architecture',
    'Romanesque architecture',
    'Russian Revival architecture',
    'Tudor Revival architecture',
]

def count_architectures(dir):
    architecture_count = 0
    for file in os.listdir(dir):
        if file in LABELS:
            architecture_count += 1
    return architecture_count

print(count_architectures(ROOT_DATASET_DIR), "architectures found")

def count_total_images(dir):
    total_images = 0
    for file in os.listdir(dir):
        if file in LABELS:
            images = os.listdir(dir + file)
            print(file, len(images))
            total_images += len(images)

    return total_images

print("total images", count_total_images(ROOT_DATASET_DIR))

def get_minimum_images(dir):
    sizes = []
    for file in os.listdir(dir):
        if file in LABELS:
            images = os.listdir(ROOT_DATASET_DIR + file)
            sizes.append(len(images))

    return min(sizes)

def get_maximum_images(dir):
    sizes = []
    for file in os.listdir(dir):
        if file in LABELS:
            images = os.listdir(ROOT_DATASET_DIR + file)
            sizes.append(len(images))

    return max(sizes)

print("image min", get_minimum_images(ROOT_DATASET_DIR))
print("image max", get_maximum_images(ROOT_DATASET_DIR))

def copy_architecture(source_dir, dest_dir, max_images):
    images = os.listdir(source_dir)
    random.shuffle(images)
    count = 0
    for image in images:
        if count > max_images:
            break

        shutil.copy2(source_dir + "/" + image, dest_dir)
        count += 1

def balance_dataset():
    min = get_minimum_images(ROOT_DATASET_DIR)

    if not os.path.isdir(OUTPUT_DATASET_DIR):
        os.mkdir(OUTPUT_DATASET_DIR)

    for architecture in LABELS:
        if not os.path.isdir(OUTPUT_DATASET_DIR + architecture):
            os.mkdir(OUTPUT_DATASET_DIR + architecture)
        copy_architecture(ROOT_DATASET_DIR + architecture + "/", OUTPUT_DATASET_DIR + architecture + "/", min)

balance_dataset()

print("after balance total images", count_total_images(OUTPUT_DATASET_DIR))